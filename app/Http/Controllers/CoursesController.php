<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function index(){
        return view('admin.courses',[
            'courses' => Course::all()
        ]);
    }

    public function create(){
        return view('admin.create_course');

    }

    public function store(Request $request){
        $formFields = $request->validate([
            'title' => 'required|max:255|min:3',
            'batch_no' => 'required|unique:courses|max:255|',
            'course_type' => 'required|max:255|min:3',
            'instructor_name' => 'nullable',
            'class_start_date' => 'required|date|after:tomorrow',
            'class_end_date' => 'required|date|after:start_date'
            
        ]); 
        
        $formFields['is_active'] = $request->is_active ?? '0';
        
        if ($request->hasFile('banner')) {
            $formFields['banner'] = $request->file('banner')->store('banner','public');
        }

        Course::create($formFields);

        return redirect()->route('courses.home')->with('message','Course Creatd Successfully');
    }

    public function edit(Course $course){
        return view('admin.edit_courses',compact('course'));
    }

    public function update(Request $request,Course $course){
        $formFields = $request->validate([
            'title' => 'required|max:255|min:3',
            'batch_no' => 'required|max:255|',
            'course_type' => 'required|max:255|min:3',
            'instructor_name' => 'nullable',
            'class_start_date' => 'required|date|after:tomorrow',
            'class_end_date' => 'required|date|after:start_date'
            
        ]); 
        
        $formFields['is_active'] = $request->is_active ?? '0';
        
        if ($request->hasFile('banner')) {
            $formFields['banner'] = $request->file('banner')->store('banner','public');
        }

        $course->update($formFields);

        return redirect()->route('courses.home')->with('message','Course Updated Successfully');
    }
    
    public function destroy(Course $course){
        $course->delete();
        return redirect()->route('courses.home')->with('message','Course Deleted Successfully');
    }
}
