<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255|min:3',
            'batch_no' => 'required|unique:courses|max:255|',
            'course_type' => 'required|max:255|min:3',
            'instructor_name' => 'nullable',
            'class_start_date' => 'required|date|after:tomorrow',
            'class_end_date' => 'required|date|after:start_date'
        ];
    }
}
