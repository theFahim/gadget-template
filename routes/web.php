<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CoursesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PublicController::class,'index']);
Route::get('/store', [PublicController::class,'store']);
Route::get('/product', [PublicController::class,'product']);
Route::get('/cart', [PublicController::class,'cart']);
Route::get('/checkout', [PublicController::class,'checkout']);
Route::get('/order', [PublicController::class,'orderComplete']);


Route::middleware(['auth'])->name('dashboard.')->group(function (){
    Route::get('admin/dashboard',[DashboardController::class,'home'])->name('home');

    Route::get('/dashboard/user', [DashboardController::class,'user'])->name('users');


    Route::get('/dashboard/products', [DashboardController::class,'products'])->name('products');
    Route::get('/dashboard/products/create', [DashboardController::class,'create'])->name('products.create');
    Route::post('/dashboard/products', [DashboardController::class,'store'])->name('products.store');

});

// Route::middleware(['auth'])->name('courses.')->group(function () {
//     Route::get('/courses', [CoursesController::class,'index'])->name('home');
//     Route::get('/courses/create', [CoursesController::class,'create'])->name('create');
//     Route::post('/courses', [CoursesController::class,'store'])->name('store');
//     Route::get('/courses/{course}/edit', [CoursesController::class, 'edit'])->name('edit');
//     Route::patch('/courses/{course}', [CoursesController::class, 'update'])->name('update');
//     Route::delete('/courses/{course}',[CoursesController::class,'destroy'])->name('delete');
// });

Route::middleware(['auth'])->resource('categories',CategoryController::class);

Route::get('/login',[AuthController::class,'login']);
Route::get('/registration',[AuthController::class,'registration']);


Route::get('/', function () {
    return view('welcome');
});



require __DIR__.'/auth.php';
