<div class="header-bottom sticky-header fix-top sticky-content">
                <div class="container justify-content-center">
                    <div class="row align-items-center gutter-no scrollable">
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media" style="font-size: 3.8rem">
                                <i class="d-icon-mobile"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Mobile Phone <br />&amp; Accessories</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media">
                                <i class="d-icon-camera1"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Camera, Photo<br /> &amp; Accessories</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media">
                                <i class="d-icon-headphone"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Portable <br />Headphone</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media">
                                <i class="d-icon-desktop"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Computers <br />&amp; Tablets</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media">
                                <i class="d-icon-cook"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Home, Kitchen <br />Electronics</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media">
                                <i class="d-icon-table-tv"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Home Video <br />&amp; Theater</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media" style="font-size: 3.3rem;">
                                <i class="d-icon-gamepad2"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Gaming &amp; <br />Accessories</h4>
                            </div>
                        </a>
                        <a href="/store" class="category category-icon-inline">
                            <div class="category-media">
                                <i class="d-icon-category"></i>
                            </div>
                            <div class="category-content">
                                <h4 class="category-name">Browse <br />More</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>