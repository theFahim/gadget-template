<x-frontend.layout.master>

<x-slot:title>
    Gadget store Products
</x-slot:title>

<main class="main shop">
			<!-- End PageHeader -->
			<div class="page-content pb-10 pt-4">
				<div class="container">
					<div class="shop-banner banner"
						style="background-image: url({{asset('ui/frontend')}}/images/demos/demo27/page-header.jpg); background-color: #eee;">
						<div class="banner-content">
							<h3 class="banner-subtitle mb-2 text-uppercase ls-l text-primary font-weight-bold">Up to
								25% Off
							</h3>
							<h1 class="banner-title mb-1 text-uppercase ls-m font-weight-bold">Cromebook simlicity</h1>
							<p class="font-primary ls-m text-dark">Galaxy Style</p>
							<a href="#" class="btn btn-outline btn-dark btn-rounded">Shop now</a>
						</div>
					</div>
					<nav class="toolbox toolbox-horizontal sticky-toolbox sticky-content fix-top">
						<aside class="sidebar sidebar-fixed shop-sidebar">
							<div class="sidebar-overlay">
								<a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
							</div>
							<a href="#" class="sidebar-toggle"><i class="fas fa-chevron-right"></i></a>
							<div class="sidebar-content toolbox-left">
								<div class="toolbox-item select-menu">
									<a class="select-menu-toggle" href="#">Select Size</a>
									<ul class="filter-items">
										<li><a href="#">Small</a></li>
										<li><a href="#">Medium</a></li>
										<li><a href="#">Large</a></li>
										<li><a href="#">Extra Large</a></li>
									</ul>
								</div>
								<div class="toolbox-item select-menu">
									<a class="select-menu-toggle" href="#">Select Color</a>
									<ul class="filter-items">
										<li><a href="#">Black</a></li>
										<li><a href="#">Blue</a></li>
										<li><a href="#">Green</a></li>
									</ul>
								</div>
								<div class="toolbox-item select-menu">
									<a class="select-menu-toggle" href="#">Select Price</a>
									<ul class="filter-items">
										<li><a href="#">$0.00 - $50.00</a><span class="count">(1)</span></li>
										<li><a href="#">$50.00 - $100.00</a><span class="count">(10)</span></li>
										<li><a href="#">$100.00 - $200.00</a><span class="count">(40)</span></li>
										<li><a href="#">$200.00+</a><span class="count">(11)</span></li>
									</ul>
								</div>
							</div>
						</aside>
						<div class="toolbox-left">
							<div class="toolbox-item toolbox-sort select-menu">
								<select name="orderby" class="form-control">
									<option value="default" selected="selected">Default Sorting</option>
									<option value="popularity">Most Popular</option>
									<option value="rating">Average rating</option>
									<option value="date">Latest</option>
									<option value="price-low">Sort forward price low</option>
									<option value="price-high">Sort forward price high</option>
									<option value="">Clear custom sort</option>
								</select>
							</div>
						</div>
						<div class="toolbox-right mr-0">
							<div class="toolbox-item toolbox-show select-menu">
								<select name="count" class="form-control">
									<option value="12">Show 12</option>
									<option value="24">Show 24</option>
									<option value="36">Show 36</option>
								</select>
							</div>
							<div class="toolbox-item toolbox-layout">
								<a href="shop-list-mode.html" class="d-icon-mode-list btn-layout"></a>
								<a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
							</div>
						</div>
					</nav>
					<div class="select-items">
						<a href="#" class="filter-clean text-primary">Clean All</a>
					</div>
					<div class="row cols-2 cols-sm-3 cols-md-4 cols-xl-5 product-wrapper gutter-no split-line">
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/1.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Fashion Sports Watch</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/2.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" title="Select Options"><i
												class="d-icon-arrow-right"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Fashion Sports Watch</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00->$210.00</ins>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/3.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="/product" class="btn-product-icon btn-cart"
											title="Select Options"><i class="d-icon-arrow-right"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Bluetooth Speaker</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00-$210.00</ins>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product product-variable text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/4.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="/product" class="btn-product-icon btn-cart"
											title="Select Options"><i class="d-icon-arrow-right"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Galaxy Tablets</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00-$210.00</ins>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/5.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">iPad 7</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product product-variable text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/6.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="/product" class="btn-product-icon btn-cart"
											title="Select Options"><i class="d-icon-arrow-right"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Apple Sports Watch</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00-$210.00</ins>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/7.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Huawei SmartPhone</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/8.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">XBox Gaming Pad</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/9.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Electronic Roaster</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/10.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Electornic Mixer</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/11.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Electronic Pulverizer</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/12.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Electronic Pulverizer</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>

						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/2.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Fashion Sports Watch</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/3.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Bluetooth Speaker</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="/product">
										<img src="{{asset('ui/frontend')}}/images/demos/demo27/shop/4.jpg" alt="product" width="196"
											height="173">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
										<a href="#" class="btn-product-icon btn-compare" title="Add to Comparelist"><i
												class="d-icon-compare"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<h3 class="product-name">
										<a href="/product">Galaxy Tablets</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<nav class="toolbox toolbox-pagination">
						<p class="toolbox-item show-info d-block">Showing <span>12 of 56</span> Products</p>
						<ul class="pagination">
							<li class="page-item disabled">
								<a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
									aria-disabled="true">
									<i class="d-icon-arrow-left"></i>Prev
								</a>
							</li>
							<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link page-link-next" href="#" aria-label="Next">
									Next<i class="d-icon-arrow-right"></i>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<section class="banner banner-newsletter bg-primary">
				<div class="container">
					<div class="banner-content row">
						<div class="col-lg-5 d-flex">
							<div class="icon-box icon-box-side appear-animate"
								data-animation-options="{'name': 'fadeInLeftShorter'}">
								<div class="icon-box-content">
									<h4 class="icon-box-title text-white">Subscribe to the newsletter</h4>
									<p class="text-white">Stay up to date regarding the newest product & offers</p>
								</div>
							</div>
						</div>
						<div class="col-lg-7 d-lg-block d-flex justify-content-center">
							<form action="#" method="get"
								class="input-wrapper input-wrapper-inline ml-lg-auto appear-animate"
								data-animation-options="{'name': 'fadeInRightShorter'}">
								<input type="email" class="form-control font-primary font-italic form-solid"
									name="email" id="email" placeholder="Enter Your E-mail Address..." required="">
								<button class="btn btn-dark" type="submit">Subscribe<i
										class="d-icon-arrow-right"></i></button>
							</form>
						</div>
					</div>
				</div>
			</section>
			
		</main>
</x-frontend.layout.master>