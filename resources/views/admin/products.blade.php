<x-backend.layout.master>
    <x-slot:title>
        products page
    </x-slot:title>
    <x-slot:pageTitle>
        Products
    </x-slot:pageTitle>
    <x-slot:pageIcon>
        mdi mdi-airplay
    </x-slot:pageIcon>

    <a href="{{route('dashboard.products.create')}}" class="btn btn-primary mx-3">Create New</a>

    <h6 class="container card-title m-t-40"><i
        class="m-r-5 font-18 mdi mdi-format-list-numbers"></i> Products Category List</h6>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">description</th>
                <th scope="col">is_active</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$category->title}}</td>
                    <td>{{$category->description}}</td>
                    <td>{{$category->is_active}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</x-backend.layout.master>