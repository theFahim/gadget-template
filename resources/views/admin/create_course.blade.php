<x-backend.layout.master>

<x-slot:title>
    products create page
</x-slot:title>
<x-slot:pageTitle>
    Create New Product
</x-slot:pageTitle>
<x-slot:pageIcon>
    mdi mdi-library-plus
</x-slot:pageIcon>

@if ($errors->any())
<x-backend.layout.alerts.errors />
@endif

<h5>Product caregory</h5>
<hr>
<form class="container " method="POST" action="{{ route('courses.store') }}" enctype="multipart/form-data">
    @csrf
    <x-backend.layout.forms.label for="title" text="Title"/>     
    <x-backend.layout.forms.input name="title" type="text" :value="old('title')"/>

    <x-backend.layout.forms.label for="banner" text="Banner Image"/>     
    <x-backend.layout.forms.input name="banner" type="file" />

    <x-backend.layout.forms.label for="batch_no" text="Batch Number"/>     
    <x-backend.layout.forms.input name="batch_no" type="number" :value="old('batch_no')" />

    <x-backend.layout.forms.label for="course_type" text="Course Type"/>     
    <x-backend.layout.forms.input name="course_type" type="text" :value="old('course_type')"/>

    <x-backend.layout.forms.label for="instructor_name" text="Instructor Name"/>     
    <x-backend.layout.forms.input name="instructor_name" type="text" :value="old('instructor_name')"/>

    <x-backend.layout.forms.label for="class_start_date" text="Class Start Date"/>     
    <x-backend.layout.forms.input name="class_start_date" type="date" :value="old('class_start_date')"/>

    <x-backend.layout.forms.label for="class_end_date" text="Class End Date"/>     
    <x-backend.layout.forms.input name="class_end_date" type="date" :value="old('class_end_date')"/>
    
    <div class="form-check">
        <input name="is_active" class="form-check-input" type="checkbox" value="1" id="isActiveInput" checked>
        <label class="form-check-label" for="isActiveInput">
            Is Active
        </label>
    </div>

    <button class="btn btn-primary py-2 px-3"> Create</button>
</form>




</x-backend.layout.master>