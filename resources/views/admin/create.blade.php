<x-backend.layout.master>

<x-slot:title>
    products create page
</x-slot:title>
<x-slot:pageTitle>
    Create New Product
</x-slot:pageTitle>
<x-slot:pageIcon>
    mdi mdi-library-plus
</x-slot:pageIcon>

<h5>Product caregory</h5>
<hr>
<form method="POST" action="{{route('dashboard.products.store')}}">
    @csrf
        <div class="form-group has-validation">
          <label for="title">Category Title</label>
          <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name ="title" placeholder="Enter Title">
          @error('title')
          <p class="form-text text-danger">
              {{$message}}
          </p>
          @enderror
        </div>

        <div class="form-group">
            <textarea name="description" class="form-control is_invalid" id="description" cols="20" rows="10"></textarea>
        @error('description')
            <p>
                {{$message}}
            </p>
        @enderror
        </div>
        <div class="form-check">
          <input type="checkbox" class="form-check-input" id="is_active" name="is_active" value="1" >
          <label class="form-check-label" for="is_active" >is active</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
</form>




</x-backend.layout.master>