<x-backend.layout.master>

    <x-slot:title>
        Create Categories Page
    </x-slot:title>
    <x-slot:pageTitle>
        Create New Categories
    </x-slot:pageTitle>
    <x-slot:pageIcon>
        mdi mdi-library-plus
    </x-slot:pageIcon>
    
    @if ($errors->any())
    <x-backend.layout.alerts.errors />
    @endif
    
    <h5>Product caregory</h5>
    <hr>
    <form class="container " method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
        @csrf   
        <x-backend.layout.forms.input name="title" type="text" :value="old('title')"/>
        
        <x-backend.layout.forms.textarea name="description" id="description" type="number" :value="old('description')" />
   
        <x-backend.layout.forms.input name="icon" id="icon" type="text" :value="old('icon')"/>
       
        <button class="btn btn-primary py-2 px-3"> Create</button>
    </form>
    
    
    
    
    </x-backend.layout.master>