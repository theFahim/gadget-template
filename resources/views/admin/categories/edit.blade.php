<x-backend.layout.master>

    <x-slot:title>
        Edit Categories Page
    </x-slot:title>
    <x-slot:pageTitle>
        Edit Categories
    </x-slot:pageTitle>
    <x-slot:pageIcon>
        mdi mdi-sitemap
    </x-slot:pageIcon>
    
    @if ($errors->any())
        <x-backend.layout.alerts.errors />
    @endif
    
    <h5>Edit caregory</h5>
    <hr>
    <form class="container " method="POST" action="{{ route('categories.update',['category'=>$category->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('patch')
        <x-backend.layout.forms.input name="title" type="text" :value="old('title',$category->title)"/>
        
        <x-backend.layout.forms.textarea name="description" id="description" type="number" :value="old('description',$category->description)" />
   
        <x-backend.layout.forms.input name="icon" id="icon" type="text" :value="old('icon',$category->icon)"/>
       
        <button class="btn btn-primary py-2 px-3"> Update</button>
    </form>
    
    
    
    
    </x-backend.layout.master>