<x-backend.layout.master>
    <x-slot:title>
        Courses page
    </x-slot:title>
    <x-slot:pageTitle>
        Courses
    </x-slot:pageTitle>
    <x-slot:pageIcon>
        mdi mdi-library-books
    </x-slot:pageIcon>

    <a href="{{route('courses.create')}}" class="btn btn-primary mx-3"><i class="mdi mdi-plus"></i> Create New</a>
    <a href="{{route('courses.create')}}" class="btn btn-secondary mx-3"> <i class="mdi mdi-delete-sweep"></i> Trash Can</a>

    @if ($errors->any())
    <x-backend.layout.alerts.errors />
    @endif

    <x-backend.layout.alerts.message type="success" :message="session('message')" class="mt-2" />

    <h6 class="container card-title m-t-40"><i
        class="m-r-5 font-18 mdi mdi-format-list-numbers"></i> Courses List</h6>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">banner</th>
                <th scope="col">batch no</th>
                <th scope="col">course type</th>
                <th scope="col">instructor name</th>
                <th scope="col">Is Active</th>
                <th scope="col">class start date</th>
                <th scope="col">class end date</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($courses as $course)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$course->title}}</td>
                    <td><img
                        src="{{$course->banner ? asset('storage/'.$course->banner) : 'image empty'}}"
                        alt="course-banner" width="50px"
                    /></td>
                    <td>{{$course->batch_no}}</td>
                    <td>{{$course->course_type}}</td>
                    <td>{{$course->instructor_name}}</td>
                    <td>{{$course->is_active?'acive':'inactive'}}</td>
                    <td>{{$course->class_start_date}}</td>
                    <td>{{$course->class_end_date}}</td>
                    <td style="display: inline">

                        <a href="{{ route('courses.edit', ['course' => $course->id]) }}"><button type="button" class="btn btn-outline-primary"><i class="mdi mdi-grease-pencil"></i> Edit</button></a>

                        <form method="POST" action="{{ route('courses.delete', ['course' => $course->id]) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-outline-danger"><i class="mdi mdi-delete"></i> Delete</button>
                        </form>

                    </td>



                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</x-backend.layout.master>